package com.WarwickWestonWright.FloatingKeyboardDemo;

import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import info.gargy.floatingkeyboard.FloatingKeyboardView;

public class MainActivity extends AppCompatActivity implements TestDialogFragment.ITestDialogFragment {

	private EditText txtKeyboardDemo;
	private TestDialogFragment testDialogFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		/* Uncomment code to see library working fromm a dialog fragment
		testDialogFragment = new TestDialogFragment();
		testDialogFragment.setCancelable(true);
		testDialogFragment.show(getSupportFragmentManager(), "TestDialogFragment");
		*/
		txtKeyboardDemo = findViewById(R.id.txtKeyboardDemo);
		FloatingKeyboardView floatingKeyboardView = findViewById(R.id.keyboardview);
		floatingKeyboardView.setKeyboard(new Keyboard(this, R.xml.keyboard_number));
		floatingKeyboardView.setPreviewEnabled(false);//NOTE Do not show the preview balloons
		floatingKeyboardView.registerEditText(txtKeyboardDemo);
	}

	@Override
	public void testDialogFragmentCb() {}
}