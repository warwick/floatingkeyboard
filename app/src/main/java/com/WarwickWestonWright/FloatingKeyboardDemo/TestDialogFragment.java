package com.WarwickWestonWright.FloatingKeyboardDemo;

import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import info.gargy.floatingkeyboard.FloatingKeyboardView;

public class TestDialogFragment extends DialogFragment {

	private View rootView;
	private EditText txtKeyboardDemo;

	public interface ITestDialogFragment {void testDialogFragmentCb();}
	private ITestDialogFragment iTestDialogFragment;

	public TestDialogFragment() {}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(iTestDialogFragment == null) {
			iTestDialogFragment = (ITestDialogFragment) getTargetFragment();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.test_dialog_fragment, container, false);
		txtKeyboardDemo = rootView.findViewById(R.id.txtKeyboardDemo);
		FloatingKeyboardView floatingKeyboardView = rootView.findViewById(R.id.keyboardview);
		floatingKeyboardView.setKeyboard(new Keyboard(getContext(), R.xml.keyboard_number));
		floatingKeyboardView.setPreviewEnabled(false);//NOTE Do not show the preview balloons
		floatingKeyboardView.registerEditText(txtKeyboardDemo);

		return rootView;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof ITestDialogFragment) {
			iTestDialogFragment = (ITestDialogFragment) context;
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iTestDialogFragment = null;
	}

}