/**
 * Copyright 2017 George Argyrakis
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p>
 * If you use this software in a product, an acknowledgment in the product
 * documentation would be appreciated but is not required.
 */
package info.gargy.floatingkeyboard;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.inputmethodservice.KeyboardView;
import android.text.Editable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class FloatingKeyboardView extends KeyboardView implements
	KeyboardView.OnKeyboardActionListener,
	View.OnFocusChangeListener,
	View.OnClickListener,
	View.OnTouchListener {
	private static final int MOVE_THRESHOLD = 0;
	private static final int TOP_PADDING_DP = 28;
	private static final int HANDLE_COLOR = Color.parseColor("#AAD1D6D9");
	private static final int HANDLE_PRESSED_COLOR = Color.parseColor("#D1D6D9");
	private static final float HANDLE_ROUND_RADIOUS = 20.0F;
	private static final CornerPathEffect HANDLE_CORNER_EFFECT = new CornerPathEffect(20.0F);
	private int topPaddingPx;
	private int width;
	private Path path;
	private Paint paint;
	private Window targetWindow;
	private EditText focusedView;
	private InputMethodManager imm;
	//Used by onTouchListener
	float dx;
	float dy;
	int moveToY;
	int moveToX;
	int distY;
	int distX;
	Rect inScreenCoordinates;
	boolean handleTouched = false;
	//KeyCode Constants
	final static int CodeGrab = -10;
	final static int CodeDelete = -5;
	final static int CodeCancel = -3;
	final static int CodePrev = 55000;
	final static int CodeAllLeft = 55001;
	final static int CodeLeft = 55002;
	final static int CodeRight = 55003;
	final static int CodeAllRight = 55004;
	final static int CodeNext = 55005;
	final static int CodeClear = 55006;
	final static int CodeCellUp = 1001;
	final static int CodeCellDown = 1002;
	final static int CodeCellLeft = 1003;
	final static int CodeCellRight = 1004;
	final static int CodeHome = 122;
	final static int CodeEnd = 123;
	final static int CodeDecimalpoint = 46;
	final static int CodeMinus = 69;
	final static int CodeZero = 48;

	public FloatingKeyboardView(Context context, AttributeSet attrs) {
		super(context, attrs);
		topPaddingPx = (int) convertDpToPixel(28.0F, context);
		setOnKeyboardActionListener(this);
		imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		if(targetWindow == null) {
			try {
				targetWindow = ((Activity) context).getWindow();
			}
			catch (ClassCastException e) {
				targetWindow = ((Activity) ((ContextThemeWrapper) context).getBaseContext()).getWindow();
			}
		}
		targetWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		setOnTouchListener(this);
		setPadding(0, (int)convertDpToPixel(28.0F, context), 0, 0);
		paint = new Paint();
		paint.setColor(HANDLE_COLOR);
		paint.setStyle(Paint.Style.FILL);
		paint.setPathEffect(HANDLE_CORNER_EFFECT);
		path = new Path();
	}


	public void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
		super.onSizeChanged(xNew, yNew, xOld, yOld);
		width = xNew;
		drawHandle();
	}

	private void drawHandle() {
		path.rewind();
		path.moveTo(0.0F, topPaddingPx);
		path.lineTo(0.0F, topPaddingPx - 25);
		path.lineTo(width / 3, topPaddingPx - 25);
		path.lineTo(width / 3, 0.0F);
		path.lineTo(2 * width / 3, 0.0F);
		path.lineTo(2 * width / 3, topPaddingPx - 25);
		path.lineTo(width, topPaddingPx - 25);
		path.lineTo(width, topPaddingPx);
		path.lineTo(width, topPaddingPx);
	}

	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		Paint paint = this.paint;
		Path path = this.path;
		canvas.drawPath(path, paint);
	}

	public void show(View view) {
		focusedView = (EditText) view;
		if(focusedView != null) {
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			setEnabled(true);
			setVisibility(View.VISIBLE);
			bringToFront();
		}
	}

	public void hide() {
		setVisibility(View.GONE);
		setEnabled(false);
	}

	@Override
	public void onFocusChange(View view, boolean hasFocus) {
		if(hasFocus) {
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			show(view);
		}
		else {
			hide();
		}
	}

	@Override
	public void onClick(View view) {
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		show(view);
		focusedView = (EditText) view;
		focusedView.setCursorVisible(true);
	}

	public void registerEditText(EditText view) {
		view.setOnFocusChangeListener(this);
		view.setOnClickListener(this);
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {

		ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
		boolean performClick = false;
		switch (event.getAction()) {
			case 2:
				if(handleTouched) {
					moveToY = ((int)(event.getRawY() - dy));
					moveToX = ((int)(event.getRawX() - dx));
					distY = (moveToY - params.topMargin);
					distX = (moveToX - params.leftMargin);
					if((Math.abs(distY) > 0) ||
						(Math.abs(distX) > 0)) {
						moveToY -= Integer.signum(distY) * Math.min(0, Math.abs(distY));
						moveToX -= Integer.signum(distX) * Math.min(0, Math.abs(distX));

						inScreenCoordinates = keepInScreen(moveToY, moveToX);
						view.setY(inScreenCoordinates.top);
						view.setX(inScreenCoordinates.left);
					}
					performClick = false;
				}
				else {
					performClick = true;
				}
				break;
			case 1:
				if(handleTouched) {
					paint.setColor(FloatingKeyboardView.HANDLE_COLOR);
					paint.setStyle(Paint.Style.FILL);
					invalidate();
					performClick = false;
				}
				else {
					performClick = true;
				}
				break;
			case 0:
				handleTouched = (event.getY() <= getPaddingTop());
				dy = (event.getRawY() - view.getY());
				dx = (event.getRawX() - view.getX());
				if(handleTouched) {
					paint.setColor(FloatingKeyboardView.HANDLE_PRESSED_COLOR);
					paint.setStyle(Paint.Style.FILL);
					invalidate();
					performClick = false;
				}
				else {
					performClick = true;
				}
				break;
		}
		return !performClick;

	}

	private void moveTo(int y, int x) {
		ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams)getLayoutParams();
		params.topMargin = y;
		params.leftMargin = x;
		setLayoutParams(params);
	}

	public void positionTo(int x, int y) {
		moveTo(y, x);
	}

	private Rect keepInScreen(int topMargin, int leftMargin) {
		int top = topMargin;
		int left = leftMargin;
		measure(-2, -2);
		int height = getMeasuredHeight();
		int width = getMeasuredWidth();

		int rightCorrection = ((View)getParent()).getPaddingRight();
		int botomCorrection = ((View)getParent()).getPaddingBottom();
		int leftCorrection = ((View)getParent()).getPaddingLeft();
		int topCorrection = ((View)getParent()).getPaddingTop();

		Rect rootBounds = new Rect();
		((View)getParent()).getHitRect(rootBounds);
		rootBounds.set(rootBounds.left + leftCorrection, rootBounds.top + topCorrection, rootBounds.right - rightCorrection, rootBounds.bottom - botomCorrection);
		if(top <= rootBounds.top) {
			top = rootBounds.top;
		}
		else if(top + height > rootBounds.bottom) {
			top = rootBounds.bottom - height;
		}
		if(left <= rootBounds.left) {
			left = rootBounds.left;
		}
		else if(left + width > rootBounds.right) {
			left = rootBounds.right - width;
		}
		return new Rect(left, top, left + width, top + height);
	}

	private static float convertDpToPixel(float dp, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160.0F);
		return px;
	}

	@Override
	public void onKey(int primaryCode, int[] keyCodes) {

		long eventTime = System.currentTimeMillis();
		if((focusedView == null) || ((focusedView.getClass() != EditText.class) && (focusedView.getClass().getSuperclass() != EditText.class))) {
			return;
		}
		Editable editable = focusedView.getText();
		int start = focusedView.getSelectionStart();
		int end = focusedView.getSelectionEnd();
		if(primaryCode == CodeCancel) {
			hide();
		}
		else if(primaryCode == CodeDelete) {
			if((editable != null) && (start > 0)) {
				editable.delete(start - 1, start);
			}
			else if((editable != null) && (start != end)) {
				editable.delete(start, end);
			}
		}
		else if(primaryCode == CodeClear) {
			if(editable != null) {
				editable.clear();
			}
		}
		else if(primaryCode == CodeLeft) {
			if(start > 0) {
				focusedView.setSelection(start - 1);
			}
		}
		else if(primaryCode == CodeRight) {
			if(start < focusedView.length()) {
				focusedView.setSelection(start + 1);
			}
		}
		else if(primaryCode == CodeAllLeft) {
			focusedView.setSelection(0);
		}
		else if(primaryCode == CodeAllRight) {
			focusedView.setSelection(focusedView.length());
		}
		else if(primaryCode == CodePrev) {
			View focusNew = focusedView.focusSearch(17);
			if(focusNew != null) {
				focusNew.requestFocus();
			}
		}
		else if(primaryCode == CodeNext) {
			View focusNew = focusedView.focusSearch(66);
			if(focusNew != null) {
				focusNew.requestFocus();
			}
			else if((primaryCode != CodeCellUp) && (primaryCode != CodeCellDown) && (primaryCode != CodeCellLeft) && (primaryCode != CodeCellRight)) {
				if(primaryCode != CodeGrab) {}
			}
		}
		else if(primaryCode == CodeMinus) {
			if(focusedView.getSelectionStart() == 0) {
				editable.insert(start, "-");
			}
		}
		else if(primaryCode == CodeHome) {
			focusedView.setSelection(0);
		}
		else if(primaryCode == CodeEnd) {
			focusedView.setSelection(focusedView.getText().length());
		}
		else {
			if(start != end) {
				editable.delete(start, end);
			}
			editable.insert(start, Character.toString((char) primaryCode));
		}
		KeyEvent event = new KeyEvent(eventTime, eventTime, 0, primaryCode, 0, 0, 0, 0, 6);

		dispatchKeyEvent(event);

	}
	@Override
	public void onPress(int primaryCode) {}
	@Override
	public void onRelease(int primaryCode) {}
	@Override
	public void onText(CharSequence text) {}
	@Override
	public void swipeLeft() {}
	@Override
	public void swipeRight() {}
	@Override
	public void swipeDown() {}
	@Override
	public void swipeUp() {}
}