###Class usage###



```
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import info.gargy.floatingkeyboard.FloatingKeyboardView;

public class MainActivity extends AppCompatActivity implements TestDialogFragment.ITestDialogFragment {

	private EditText txtKeyboardDemo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		txtKeyboardDemo = findViewById(R.id.txtKeyboardDemo);
		FloatingKeyboardView floatingKeyboardView = findViewById(R.id.keyboardview);
		floatingKeyboardView.setKeyboard(new Keyboard(this, R.xml.keyboard_number));
		floatingKeyboardView.setPreviewEnabled(false);//NOTE Do not show the preview balloons
		floatingKeyboardView.registerEditText(txtKeyboardDemo);
	}

	@Override
	public void testDialogFragmentCb() {}
}
```

###XML Layout main_activity.xml###

```
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout
    android:id="@+id/rLayoutRootViewMain"
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <requestFocus />

    <TextView
        android:id="@+id/lblTitle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:gravity="center"
        android:textSize="18dp"
        android:textStyle="bold"
        android:textColor="@android:color/black"
        android:layout_alignParentTop="true"
        android:layout_centerHorizontal="true"
        android:text="@string/app_name"/>

    <EditText
        android:id="@+id/txtKeyboardDemo"
        style="@style/SettingsEditText"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:gravity="center"
        android:inputType="text"
        android:layout_alignParentTop="true"
        android:layout_centerHorizontal="true"
        android:layout_marginLeft="8dp"
        android:layout_marginTop="50dp"
        android:layout_marginRight="8dp"
        android:cursorVisible="true"
        android:textSize="18dp"
        android:textStyle="bold"
        android:textColor="@android:color/black"
        android:layout_below="@+id/lblTitle"/>

    <info.gargy.floatingkeyboard.FloatingKeyboardView
        android:id="@+id/keyboardview"
        android:layout_width="300dp"
        android:layout_height="240dp"
        android:focusable="true"
        android:focusableInTouchMode="true"
        android:visibility="gone"
        android:keyTextColor="#384850"
        android:keyBackground="@drawable/keyback"
        android:shadowRadius="0.0"
        android:background="@android:color/transparent"
        android:layout_alignParentBottom="true"
        android:layout_centerHorizontal="true"
        android:elevation="1dp"/>

</RelativeLayout>
```


###Add keyboard layout file to. res.mxl.keyboard_number.xml###


```
<?xml version="1.0" encoding="utf-8"?>
<Keyboard xmlns:android="http://schemas.android.com/apk/res/android"
    android:keyWidth="60dp"
    android:keyHeight="60dp"
    android:horizontalGap="0dp"
    android:verticalGap="0dp">
    <Row>
        <Key android:codes="49" android:keyLabel="1" android:keyEdgeFlags="left" />
        <Key android:codes="50" android:keyLabel="2" />
        <Key android:codes="51" android:keyLabel="3" />
        <Key android:codes="46" android:keyLabel="." android:keyEdgeFlags="right" />
    </Row>

    <Row>
        <Key android:codes="52" android:keyLabel="4" android:keyEdgeFlags="left" />
        <Key android:codes="53" android:keyLabel="5" />
        <Key android:codes="54" android:keyLabel="6" />
        <Key android:codes="69" android:keyLabel="-" android:keyEdgeFlags="right" />
    </Row>

    <Row>
        <Key android:codes="55" android:keyLabel="7" android:keyEdgeFlags="left" />
        <Key android:codes="56" android:keyLabel="8" />
        <Key android:codes="57" android:keyLabel="9" />
        <Key android:codes="-5" android:keyIcon="@drawable/bckspc" android:isRepeatable="true" android:keyEdgeFlags="right" />
    </Row>

    <Row>
        <Key android:codes="122" android:keyLabel="\u21e6" android:keyEdgeFlags="left" />
        <Key android:codes="48" android:keyLabel="0" />
        <Key android:codes="123" android:keyLabel="\u21e8" />
        <Key android:codes="-3" android:keyLabel="\u2713" android:isRepeatable="true" android:keyEdgeFlags="right" />
    </Row>
</Keyboard>
```


You can find the key codes here: https://developer.android.com/reference/android/view/KeyEvent

###License###

####Copyright 2017 George Argyrakis####
####Licensed under the Apache License, Version 2.0 (the "License");####
####you may not use this file except in compliance with the License.####
####You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0####
####Unless required by applicable law or agreed to in writing, software####
####distributed under the License is distributed on an "AS IS" BASIS,####
####WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.####
####See the License for the specific language governing permissions and limitations under the License.####
####If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.####